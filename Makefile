#------- Project settings -------
## Naming
PROJECT_NAME             := bench

## Directories
# `BUILD_DIR` is global, others are prefix-specific
BUILD_DIR                ?= build
SRC_DIR                  := src

## Compilers (global)
CC                       := gcc
CXX                      := g++
LD                       := g++

## Compiling options (prefix-specific)
LIBS                     :=
CPPFLAGS                 +=
CFLAGS                   += -O3
CXXFLAGS                 +=
LDFLAGS                  +=
INCLUDE_DIR              :=

#------- Internal Makefile -------
include internals.mk

#------- Build and test targets -------
# Must be after `internals.mk` include
# Must be constructed from `$(BUILD_DIR)`
OUT                      := $(PROJECT_NAME)

#------- Include other parts of the build system -------
# See comments in the `build.mk` files for instructions
include $(SRC_DIR)/build.mk
#include $(TESTSRC_DIR)/build.mk

#------- Define `ALL_[PREFIX]OBJS` files -------
$(eval $(call set-objs-vars))

## Combine `ALL_[PREFIX]OBJS` files into makefile-ready targets
OBJS            := $(ALL_OBJS)

## Actual (prefix-driven) generation of rules
$(eval $(call generate-compile-rules))

#------- Internal Makefile -------
$(eval $(call end-compile-rules))

#------- Project-specific targets -------
## `all` and `test` definition
all: $(OUT)

## File targets
$(OUT): $(OBJS) | $$(@D)/.
	$(call log,green,linking,$(notdir $(@)))
	$(Q)$(LD) -o $(@) $(LDFLAGS) $^

## Clean
.PHONY: clean veryclean

clean::
	$(call log,yellow,cleaning,intermediate files)
	-@rm $(OBJS) $(OUT) 2>/dev/null | true

veryclean:
	$(call log,yellow,cleaning,all generated files)
	-@rm -rf $(BUILD_DIR) 2>/dev/null | true
