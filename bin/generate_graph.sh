#! /bin/bash
if [ "$4" == "" ]
then
	echo "usage : $0 <NAME> <FILE_PREFIX> <TOTAL_CORES> <SOCKET_CORES>"
	exit
fi


FILENAME=/tmp/`echo $1 | sed "s/ /_/g"`
cp ../templates/graphes.templateRmd $FILENAME.Rmd
NAME=$1
FILE=$2
TOTAL_CORES=$3
CORES_HT=$(($TOTAL_CORES - 1))
CORES_NOHT=$(($TOTAL_CORES / 2))
CORES_NOHT1=$(($CORES_NOHT + 1))
CORES_NOHTM1=$(($CORES_NOHT - 1))
CORES_SOCKET=$4
CORES_SOCKET1=$((CORES_SOCKET + 1))
CORES_SOCKETM1=$((CORES_SOCKET - 1))

sed -i "s/NAME/$1/g" $FILENAME.Rmd
sed -i "s%FILE%${FILE}%g" $FILENAME.Rmd
sed -i "s/CORES_HT/${CORES_HT}/g" $FILENAME.Rmd
sed -i "s/CORES_NOHT-1/${CORES_NOHTM1}/g" $FILENAME.Rmd
sed -i "s/CORES_NOHT1/${CORES_NOHT1}/g" $FILENAME.Rmd
sed -i "s/CORES_NOHT/${CORES_NOHT}/g" $FILENAME.Rmd
sed -i "s/CORES_SOCKET-1/${CORES_SOCKETM1}/g" $FILENAME.Rmd
sed -i "s/CORES_SOCKET1/${CORES_SOCKET1}/g" $FILENAME.Rmd
sed -i "s/CORES_SOCKET/${CORES_SOCKET}/g" $FILENAME.Rmd
R -e "require(rmarkdown);rmarkdown::render('$FILENAME.Rmd')"
