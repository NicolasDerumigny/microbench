## General (exported) Variables
BUILD_SYSTEM_VERSION := 1.0

NB_CPUS         := $(shell nproc)

## Supplementary compilers (also exported)
LIBTOOL_CC      := libtool --mode=compile --quiet $(CC)
LIBTOOL_CXX     := libtool --mode=compile --quiet $(CXX)
LIBTOOL_LD      := libtool --mode=link --quiet $(LD)
LIBTOOL_INSTALL := libtool --mode=install --quiet install
LIBTOOL_FINISH  := libtool --finish --quiet

## Directories (switch to absolute ones)
MAKE_ROOT                 := $(realpath $(lastword $(MAKEFILE_LIST)))
MAKE_ROOT_DIR             := $(realpath $(dir $(MAKE_ROOT)))
ifeq ($(if $(patsubst /%,,$(BUILD_DIR)),,yes),)
BUILD_DIR                 := $(MAKE_ROOT_DIR)/$(BUILD_DIR)
endif
OBJ_DIR                   := $(BUILD_DIR)/obj
DEP                       := $(BUILD_DIR)/deps

## Verbosity
VERBOSE ?= 1
V       ?= $(VERBOSE)
COLOR   ?= 1

## Colored outputs
ifneq ($(COLOR),0)
red     := \e[0;91m
green   := \e[0;92m
yellow  := \e[0;93m
blue    := \e[0;94m
magenta := \e[0;95m
cyan    := \e[0;96m
black   := \e[0m
endif

ifneq ($(filter 0 1,$(V)),)
# (Q)uiet
Q       := @
endif

#------- Internal function definition -------
ifneq ($(filter 1 3,$(V)),)
log     = $(call print,$(1),$(2),$(3))
endif

print   = @printf "$(value $1)%20s    $(black) %s\n" "$(2)" "$(3)" || exit 0

ifneq ($(filter 4,$(V)),)
dprint=$(info $(1))
endif

gen-rule = $(call dprint,$(1)) $(1)

## With `define`
# Single dollar = first expansion (`call`)
# Double dollar = second exanpsion (`eval`)

# Usage: `$(eval $(call _rel-to-abs,VAR_NAME))`
define _rel-to-abs
$(if $(strip $($(1))),,$(error "$(1)" must be defined))
ifeq ($$(if $$(patsubst /%,,$$($(1))),,yes),)
$$(eval $(1) := $$(MAKE_ROOT_DIR)/$$($(1)))
endif
endef

#------- Exported function definitions -------
# Internal function variables are prefixed with `_`

## Dependency handeling
# Usage: `$(eval $(call compile-rule,target,source,command))`
define compile-rule
$(call dprint,Generating rules for '$(1)':)
$(call gen-rule,$(1): $(2) | $$$$(@D)/.
	$$(call log,cyan,compiling,$$(notdir $$<))
	$$(Q)( [ -f $$(DEP) ] && grep -w "$$(@D)/$$(*F).d" $$(DEP) >/dev/null ) || echo -p '-include $$(@D)/$$(*F).d\nclean::\n\t$$$$(Q)rm -f $$(@D)/$$(*F).d $$@' >> $$(DEP)
	$$(Q)$(3) -MMD -MP -MT "$$@" -MF "$$(@D)/$$(*F).d" -c "$$<" -o "$$@")
endef

## Include files utilities
# Usage: `$(eval $(call set-cur-dirs-vars))`
define set-cur-dirs-vars
$$(eval CUR_DIR := $$(realpath $$(dir $$(realpath $$(lastword $$(MAKEFILE_LIST))))))
ifeq ($$(MAKE_ROOT_DIR),$$(CUR_DIR))
$$(eval CUR_REL_DIR := )
else
$$(eval CUR_REL_DIR := $$(patsubst $$(MAKE_ROOT_DIR)/%,%,$$(CUR_DIR)))
endif
endef

# Usage: `$(eval $(call update-src-vars,PREFIX))`
define update-src-vars
$$(eval $$(call _rel-to-abs,$(1)SRC_DIR))
$$(eval _ASM_$(1)SRCS := $$(_ASM_$(1)SRCS) $$(addprefix $$(CUR_DIR)/, $$(ASM_$(1)SRCS)))
$$(eval _C_$(1)SRCS   := $$(_C_$(1)SRCS) $$(addprefix $$(CUR_DIR)/, $$(C_$(1)SRCS)))
$$(eval _CXX_$(1)SRCS := $$(_CXX_$(1)SRCS) $$(addprefix $$(CUR_DIR)/, $$(CXX_$(1)SRCS)))
endef

# Usage: `$(eval $(call update-gensrc-vars,PREFIX))`
define update-gensrc-vars
$$(eval $$(call _rel-to-abs,$(1)GENSRC_DIR))
$$(eval _ASM_$(1)GENSRCS := $$(_ASM_$(1)GENSRCS) $$(addprefix $$($(1)GENSRC_DIR)/$$(CUR_REL_DIR)/, $$(ASM_$(1)GENSRCS)))
$$(eval _C_$(1)GENSRCS   := $$(_C_$(1)GENSRCS) $$(addprefix $$($(1)GENSRC_DIR)/$$(CUR_REL_DIR)/, $$(C_$(1)GENSRCS)))
$$(eval _CXX_$(1)GENSRCS := $$(_CXX_$(1)GENSRCS) $$(addprefix $$($(1)GENSRC_DIR)/$$(CUR_REL_DIR)/, $$(CXX_$(1)GENSRCS)))
endef

# Usage: `$(eval $(call end-include, PREFIX))`
define end-include
$$(eval MAKEFILE_LIST := $$(wordlist 1, $$(words $$(wordlist 2, $$(words $$(MAKEFILE_LIST)), $$(MAKEFILE_LIST))), $$(MAKEFILE_LIST)))
$$(eval $$(call set-cur-dirs-vars))
endef

## Internal vars management
# Usage: `$(eval $(call set-objs-vars-libtool,PREFIX))`
# Input: `$[PREFIX]SRC_DIR` and `$(eval $(call update-[gen]src-vars,PREFIX))`
# Output: `ALL_[PREFIX]OBJS`
define set-objs-vars-libtool
$$(eval $$(call _rel-to-abs,$(1)SRC_DIR))
$$(eval _ASM_$(1)OBJ_DIR  := $$(OBJ_DIR)/$(1)asm)
$$(eval _C_$(1)OBJ_DIR    := $$(OBJ_DIR)/$(1)c)
$$(eval _CXX_$(1)OBJ_DIR  := $$(OBJ_DIR)/$(1)cc)
$$(eval _ASM_$(1)OBJS     := $$(patsubst $$($(1)SRC_DIR)/%.S, $$(_ASM_$(1)OBJ_DIR)/%.lo, $$(_ASM_$(1)SRCS)))
$$(eval _C_$(1)OBJS       := $$(patsubst $$($(1)SRC_DIR)/%.c, $$(_C_$(1)OBJ_DIR)/%.lo,   $$(_C_$(1)SRCS)))
$$(eval _CXX_$(1)OBJS     := $$(patsubst $$($(1)SRC_DIR)/%.cc,$$(_CXX_$(1)OBJ_DIR)/%.lo, $$(_CXX_$(1)SRCS)))
$$(eval ALL_$(1)OBJS      := $$(_ASM_$(1)OBJS) $$(_C_$(1)OBJS) $$(_CXX_$(1)OBJS))
endef

# Usage: `$(eval $(call set-objs-vars,PREFIX))`
# Output `ALL_[PREFIX]OBJS`
define set-objs-vars
$$(eval $$(call _rel-to-abs,$(1)SRC_DIR))
$$(eval _ASM_$(1)OBJ_DIR  := $$(OBJ_DIR)/$(1)asm)
$$(eval _C_$(1)OBJ_DIR    := $$(OBJ_DIR)/$(1)c)
$$(eval _CXX_$(1)OBJ_DIR  := $$(OBJ_DIR)/$(1)cc)
$$(eval _ASM_$(1)OBJS     := $$(patsubst $$($(1)SRC_DIR)/%.S, $$(_ASM_$(1)OBJ_DIR)/%.o, $$(_ASM_$(1)SRCS)))
$$(eval _C_$(1)OBJS       := $$(patsubst $$($(1)SRC_DIR)/%.c, $$(_C_$(1)OBJ_DIR)/%.o,   $$(_C_$(1)SRCS)))
$$(eval _CXX_$(1)OBJS     := $$(patsubst $$($(1)SRC_DIR)/%.cc,$$(_CXX_$(1)OBJ_DIR)/%.o, $$(_CXX_$(1)SRCS)))
$$(eval ALL_$(1)OBJS      := $$(_ASM_$(1)OBJS) $$(_C_$(1)OBJS) $$(_CXX_$(1)OBJS))
endef

# Usage: `$(eval $(call generate-compile-rules,PREFIX))`
define generate-compile-rules
$$(eval _$(1)LIBFLAGS      += $$(addprefix -l, $($(1)LIBS)))
$$(eval _$(1)INCLUDEFLAGS  += $$(addprefix -I$(MAKE_ROOT_DIR)/, $($(1)INCLUDE_DIR)))
$$(eval $(1)CPPFLAGS       += $$(_$(1)INCLUDEFLAGS))
$$(eval $(1)LDFLAGS        += $$(_$(1)LIBFLAGS))
$$(eval $$(call compile-rule,$$(_ASM_$(1)OBJ_DIR)/%.lo,$$($(1)SRC_DIR)/%.S,$$(LIBTOOL_CC) $$(CPPFLAGS) $$($(1)CPPFLAGS)))
$$(eval $$(call compile-rule,$$(_C_$(1)OBJ_DIR)/%.lo,$$($(1)SRC_DIR)/%.c,$$(LIBTOOL_CC) $$(CPPFLAGS) $$($(1)CPPFLAGS) $$(CFLAGS) $$($(1)CFLAGS)))
$$(eval $$(call compile-rule,$$(_CXX_$(1)OBJ_DIR)/%.lo,$$($(1)SRC_DIR)/%.cc,$$(LIBTOOL_CXX) $$(CPPFLAGS) $$($(1)CPPFLAGS) $$(CXXFLAGS) $$($(1)CXXFLAGS)))
$$(eval $$(call compile-rule,$$(_ASM_$(1)OBJ_DIR)/%.o,$$($(1)SRC_DIR)/%.S,$$(CC) $$(CPPFLAGS) $$($(1)CPPFLAGS)))
$$(eval $$(call compile-rule,$$(_C_$(1)OBJ_DIR)/%.o,$$($(1)SRC_DIR)/%.c,$$(CC) $$(CPPFLAGS) $$($(1)CPPFLAGS) $$(CFLAGS) $$($(1)CFLAGS)))
$$(eval $$(call compile-rule,$$(_CXX_$(1)OBJ_DIR)/%.o,$$($(1)SRC_DIR)/%.cc,$$(CXX) $$(CPPFLAGS) $$($(1)CPPFLAGS) $$(CXXFLAGS) $$($(1)CXXFLAGS)))
endef

# Usage: `$(eval $(call end-compile-rules))`
define end-compile-rules
-include $$(DEP)
endef

#------- Rules -------
# Must be the first rule in the file
.SUFFIXES:        # remove all the implicit rules
.SECONDARY:       # all files are intermediate (keep them)
.SECONDEXPANSION: # usefull to automatically build obj directories
.PHONY: all       # these targets are not files
all:

.SUFFIXES:
.SECONDARY:
.SECONDEXPANSION:
.PHONY: test
test:

print-%:
	$(call print,green,$*,[$($*)])

# Automatically create directories
%/.:
	$(Q)mkdir -p $@

.PHONY: compile_commands.json
# [5,-1] accounts for the \" added by the gen-compile-rules
compile_commands.json: /usr/bin/jq
	$(call print,magenta,generating,$(@))
	$(Q)make -C $(MAKE_ROOT_DIR) test all --always-make --dry-run | grep -wE '$(CC)|$(subst +,\+,$(CXX))' \
		| sed "s/libtool --mode=compile --quiet //g" | grep -w '\-c' \
		| jq -nR '[inputs|{directory:"$(MAKE_ROOT_DIR)", command:., file: match("-c [^ ]+").string[4:-1]}]' > $(@)

.PHONY: help
help:
	@echo "usage: make target"
	@echo
	@echo "Main targets:"
	$(call print,yellow,all,generate $(PROJECT_NAME) library)
	$(call print,yellow,test,generate test files)
	$(call print,yellow,print-[var],print the variable var (e.g., print-gen-modules))
	@echo
	@echo "Main variables:"
	$(call print,green,V,verbose level (0 nothing, 1 abstract, 2 full command, 3 1+2, 4 auto debug) [$(V)])
	$(call print,green,COLOR,use colors [$(COLOR)])

.PHONY: tidy
tidy:
	$(call log,yellow,cleaning,temporary files)
	$(Q)find . \( -name "*~" -o -name "\#*" \) -exec rm {} \;
