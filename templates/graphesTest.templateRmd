---
title: "NAME Latencies measurements"
author: "Nicolas Derumigny"
date: ''
output:
  html_document: default
  html_notebook: default
---

```{r setup, include=FALSE}
library(ggplot2)
library(tidyverse)
knitr::opts_chunk$set(echo = FALSE)
path="./"

plot_csv <- function(file){
  #Thanks to https://stackoverflow.com/questions/25195869/how-to-change-color-palette-of-geom-tile-in-r-ggplot2#25196022
  dat = read.csv(paste(path, file, sep=""))
  dat.serial = gather(dat, Core2, value, X0:XCORES_HT)
  dat.serial$Core2 = as.numeric(parse_number(dat.serial$Core2))
  scale <- scale_fill_gradientn(colors=c("red", "yellow", "green",  "blue"), breaks = seq(min(dat.serial$value), max(dat.serial$value), length.out = 5), name = NULL)

  dat.noHT = dat[1:CORES_NOHT,1:CORES_NOHT1]
  dat.noHT.serial = gather(dat.noHT, Core2, value, X0:XCORES_NOHT-1)
  dat.noHT.serial$Core2 = as.numeric(parse_number(dat.noHT.serial$Core2))
  scale.noHT <- scale_fill_gradientn(colors=c("red", "yellow", "green",  "blue"), breaks = seq(min(dat.noHT.serial$value), max(dat.noHT.serial$value), length.out = 5), name = NULL)
  
  dat.onesocket = dat[1:CORES_SOCKET,1:CORES_SOCKET1]
  dat.onesocket.serial = gather(dat.onesocket, Core2, value, X0:XCORES_SOCKET-1)
  dat.onesocket.serial$Core2 = as.numeric(parse_number(dat.onesocket.serial$Core2))
  scale.onesocket <- scale_fill_gradientn(colors=c("red", "yellow", "green",  "blue"), breaks = seq(min(dat.onesocket.serial$value), max(dat.onesocket.serial$value), length.out = 5), name = NULL)
  
  
  print(ggplot(data = dat.serial) + geom_tile(aes(x=Core2,y=Core, fill=value)) + scale + ggtitle("All logical cores") + xlab("Reader") + ylab("Allocater"))
  print(ggplot(data = dat.noHT.serial) + geom_tile(aes(x=Core2,y=Core, fill=value)) + scale.noHT + ggtitle("Only physical cores") + xlab("Reader") + ylab("Allocater"))
  print(ggplot(data = dat.onesocket.serial) + geom_tile(aes(x=Core2,y=Core, fill=value)) + scale.onesocket + ggtitle("Only first socket") + xlab("Reader") + ylab("Allocater"))
}
```

## NAME - Random Read Latency
```{r, include=TRUE, echo=FALSE}
plot_csv("/FILE_rand_reads.csv")
```