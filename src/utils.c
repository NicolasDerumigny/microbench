#define _GNU_SOURCE
#include <assert.h>
#include <pthread.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <unistd.h>

#include "globals.h"
#include "utils.h"

uint64_t randl() { return ((uint64_t)rand() << 32) | rand(); }

void sort(double *numbers) {
  for (uint64_t i = 0; i < NUM_REPEAT; ++i) {
    for (uint64_t j = 1; j < NUM_REPEAT - i; ++j) {
      if (numbers[j - 1] > numbers[j]) {
        double tmp = numbers[j - 1];
        numbers[j - 1] = numbers[j];
        numbers[j] = tmp;
      }
    }
  }
}

static int get_sibling_core(int cpu_id) {
  char buf[BUFSIZ];
  sprintf(buf, "/sys/devices/system/cpu/cpu%d/topology/core_cpus_list", cpu_id);
  FILE *siblings_list = fopen(buf, "r");
  if (siblings_list == NULL) {
    fprintf(stderr, "Cannot find file for cpu %d\n", cpu_id);
    perror("Cannot find core_cpus_list file for cpu");
    exit(1);
  }
  int core1, core2;
  if (fscanf(siblings_list, "%d,%d", &core1, &core2) == 2) {
    fclose(siblings_list);
    return core1 == cpu_id ? core2 : core1;
  }
  fclose(siblings_list);
  return -1;
}

static int get_sorted_core_id(int cpu_id) {
  const int sibling = get_sibling_core(cpu_id);
  if (sibling != -1 && sibling < cpu_id) {
    return get_sorted_core_id(sibling) + 1;
  }

  int ret = 0;
  for (int i = 0; i < cpu_id; i++) {
    const int sibling_of_i = get_sibling_core(i);
    if (sibling_of_i != -1) {
      ret += 2;
    } else {
      ret += 1;
    }
  }
  return ret;
}

void sort_siblings_cores(double *numbers_raw) {
  double(*numbers)[num_CPU][NUM_REPEAT] =
      (double(*)[num_CPU][NUM_REPEAT])numbers_raw;
  double *buffer_raw =
      (double *)malloc(num_CPU * num_CPU * NUM_REPEAT * sizeof(double));
  double(*buffer)[num_CPU][NUM_REPEAT] =
      (double(*)[num_CPU][NUM_REPEAT])buffer_raw;
  int cpu_permut[num_CPU];
  for (int i = 0; i < num_CPU; i++) {
    cpu_permut[i] = get_sorted_core_id(i);
  }

  for (int i = 0; i < num_CPU; i++) {
    for (int j = 0; j < num_CPU; j++) {
      for (int k = 0; k < NUM_REPEAT; k++) {
        buffer[cpu_permut[i]][cpu_permut[j]][k] = numbers[i][j][k];
      }
    }
  }
  memcpy(numbers_raw, buffer_raw,
         num_CPU * num_CPU * NUM_REPEAT * sizeof(double));
  free(buffer_raw);
}

// Returns the median of the number assuming
// that the array is sorted
double median(double *numbers) {
  if (NUM_REPEAT % 2)
    return numbers[NUM_REPEAT / 2];
  else
    return (numbers[NUM_REPEAT / 2 - 1] + numbers[NUM_REPEAT / 2]) / 2;
}

// Returns the minimum of the number assuming
// that the array is sorted
double min(double *numbers) { return numbers[0]; }

void *allocate(uint64_t n) {
  return mmap(0, n, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, 0, 0);
}

void scramble_cache() {
  uint64_t size = 3 * L3_SIZE / 2;
  uint64_t *data = allocate(size + sizeof(uintptr_t));
  for (uint64_t i = 0; i < (size) / (sizeof(uintptr_t)); ++i)
    data[i] = (uintptr_t)randl();
  munmap(data, size + sizeof(uintptr_t));
}

void pin_cpu(uint64_t cpuid) {
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  if (cpuid == -1ul) {
    for (int i = 0; i < num_CPU; i++) {
      CPU_SET(i, &cpuset);
    }
  } else {
    CPU_SET(cpuid, &cpuset);
  }
  if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) != 0)
    perror("[WARNING] Unable to pin to CPU");
  if (cpuid == -1ul) {
    return;
  }
  while (sched_getcpu() != cpuid)
    ;
}

void check_governor() {
  for (uint32_t i = 0; i < num_CPU; ++i) {
    char path[256];
    sprintf(path, "/sys/devices/system/cpu/cpu%i/cpufreq/scaling_governor", i);
    FILE *gov = fopen(path, "r");
    if (gov == NULL) {
      fprintf(stderr, "[WARNING] Cannot open governor config file, check your "
                      "system settings\n");
      return;
    }
    fread(path, 20, 1, gov);
    if (strncmp(path, "performance", 11) != 0)
      fprintf(stderr,
              "[WARNING] Governor for core %i is not set to performance, wrong "
              "values may be reported\n",
              i);
    fclose(gov);
  }
}

void check_scheduler() {
  struct sched_param param;
  param.sched_priority = sched_get_priority_max(SCHED_FIFO);
  if (sched_setscheduler(getpid(), SCHED_FIFO, &param)) {
    perror("[WARNING] sched_setscheduler");
  }
}

struct timespec diff_timespec(const struct timespec *time1,
                              const struct timespec *time0) {
  assert(time1);
  assert(time0);
  struct timespec diff = {.tv_sec = time1->tv_sec - time0->tv_sec, //
                          .tv_nsec = time1->tv_nsec - time0->tv_nsec};
  if (diff.tv_nsec < 0) {
    diff.tv_nsec += 1000000000; // nsec/sec
    diff.tv_sec--;
  }
  return diff;
}
