#ifndef _C2C_LATENCY_H
#define _C2C_LATENCY_H

#include <stdint.h>

void benchmarking_c2c_latency(double *latencies, uint64_t repeat);

#endif // _C2C_LATENCY_H
