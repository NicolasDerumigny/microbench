#ifndef _CACHES_LATENCY
#define _CACHES_LATENCY

#include <stdint.h>

typedef enum {
  I_F, // Invalid -> Forward
  I_E, // Invalid -> Exclusive
  S_F, // Shared  -> Forward
  NUM_TRANSITION
} CacheTransitions;

void init_random(uintptr_t *data);
uintptr_t *init_memory_random();
uintptr_t *init_memory_sequential();

void benchmarking_cache_latency(double *latencies, uintptr_t *init_func(void),
                                uint64_t repeat);
void benchmarking_cache_latency_throughput(double *latencies, uint64_t repeat);
void benchmarking_cache_state_transitions(double *latencies,
                                          uintptr_t *init_func(void),
                                          uint64_t repeat);

#endif // _CACHES_LATENCY
