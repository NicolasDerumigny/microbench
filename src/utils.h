#ifndef _UTILS_H
#define _UTILS_H

#include <stdint.h>
#include <time.h>

typedef enum { MEDIAN, MIN } Agreg_t;

uint64_t randl();
void sort(double *numbers);
void sort_siblings_cores(double *numbers_raw);
double median(double *numbers);
double min(double *numbers);
void *allocate(uint64_t n);
void scramble_cache();
void pin_cpu(uint64_t cpuid);
void check_governor();
void check_scheduler();
struct timespec diff_timespec(const struct timespec *time1,
                              const struct timespec *time0);
static __inline__ uint64_t rdtscp(void) {
  uint32_t hi, lo;
  __asm__ volatile("rdtscp" : "=a"(lo), "=d"(hi));
  return ((uint64_t)lo) | (((uint64_t)hi) << 32);
}

static __inline__ uint64_t rdtsc(void) {
  uint32_t hi, lo;
  __asm__ volatile("rdtsc" : "=a"(lo), "=d"(hi));
  return ((uint64_t)lo) | (((uint64_t)hi) << 32);
}

#endif // _UTILS_H
