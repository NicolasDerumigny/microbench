#include <assert.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "c2c_latency.h"
#include "globals.h"
#include "utils.h"

// Cf https://gcc.gnu.org/wiki/Atomic/GCCMM/AtomicSync
#define __sync_load(a) __atomic_load_n(a, __ATOMIC_ACQUIRE)
#define __sync_store(a, val) __atomic_store_n(a, val, __ATOMIC_RELEASE)

uint64_t *cache_line;

typedef struct {
  uint64_t core_to_pin;
  uint64_t other_core;
  double *latencies;
  uint64_t repeat;
} slave_thread_info_t;

void *slave_thread_work(void *message_raw) {
  slave_thread_info_t *infos = (slave_thread_info_t *)message_raw;
  pin_cpu(infos->core_to_pin);
  volatile uint64_t *message = cache_line;

  struct timespec time_before;
  struct timespec time_after;

  uint64_t target = -1ul;
  while (__sync_load(message) != target) {
  }
  target++;
  __sync_store(message, target);
  target++;
  clock_gettime(CLOCK_MONOTONIC, &time_before);
  while (target < NUM_RUNS) {
    if (__sync_bool_compare_and_swap(message, target, target + 1)) {
      target += 2;
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &time_after);

  double(*lat)[num_CPU][NUM_REPEAT] =
      (double(*)[num_CPU][NUM_REPEAT])infos->latencies;

  struct timespec diff_times = diff_timespec(&time_after, &time_before);

  lat[infos->other_core][infos->core_to_pin][infos->repeat] =
      ((double)diff_times.tv_nsec) / NUM_RUNS;

  return NULL;
}

void ping_pong_master(uint64_t core_m) {
  volatile uint64_t *message = cache_line;

  uint64_t target = 0;
  __sync_store(cache_line, -1ul);
  while (target < NUM_RUNS) {
    if (__sync_bool_compare_and_swap(message, target, target + 1)) {
      target += 2;
    }
  }
}

void benchmarking_c2c_latency(double *latencies, uint64_t repeat) {
  pthread_t child;
  double(*latencies_casted)[num_CPU][NUM_REPEAT] =
      (double(*)[num_CPU][NUM_REPEAT])latencies;
  cache_line = (uint64_t *)aligned_alloc(4096, 64);
  for (uint32_t core_m = 0; core_m < num_CPU; core_m++) {
    latencies_casted[core_m][core_m][repeat] = 0.;
    for (uint64_t core_s = 0; core_s < num_CPU; core_s++) {
      if (core_s == core_m) {
        continue;
      }
      __sync_store(cache_line, -2ul);
      volatile uint64_t *message = cache_line;
      slave_thread_info_t infos = {.core_to_pin = core_s,
                                   .other_core = core_m,
                                   .latencies = latencies,
                                   .repeat = repeat};
      pin_cpu(-1);
      pthread_create(&child, NULL, slave_thread_work, &infos);
      pin_cpu(core_m);

      ping_pong_master(core_m);
      pthread_join(child, NULL);
      __sync_store(message, 0);
    }
  }
  free(cache_line);
}
