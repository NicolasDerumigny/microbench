#------- Definition of files -------
ASM_SRCS :=
C_SRCS   := c2c_latency.c    \
            caches_latency.c \
            infos.c          \
            main.c           \
            utils.c
CXX_SRCS :=

#------- Custom rules -------
$(eval $(call set-cur-dirs-vars)) # defines `CUR_DIR` and `CUR_REL_DIR`
# Guidelines:
# - Prerequistes must be absolute (i.e. use `$(CUR_DIR)`)
# - Generated files must be placed in `$([PREFIX]GENSRC_DIR)/$(CUR_REL_DIR)`
# - Custom compile rules must be defined using `$(eval $(call compile-rule,$(_CXX_[PREFIX]OBJ_DIR)/$(CUR_REL_DIR)/file,$(CUR_DIR)/file,COMMAND))`, with file containing optionnaly a `%` pattern

#------- Add the prefixed files to the build system -------
$(eval $(call update-src-vars))

#------- Inclusion of other parts of the build system -------
# in this case, copy and edit this file accordingly
#include $(CUR_DIR)/other_dir/build.mk

#------- Internal Makefile -------
$(eval $(call end-include))
