#ifndef _GLOBALS_H
#define _GLOBALS_H

#include <stdint.h>

#define ONE "movq (%5), %5\n"
#define FIVE ONE ONE ONE ONE ONE
#define TEN FIVE FIVE
#define FIFTY TEN TEN TEN TEN TEN
#define S_64 FIFTY TEN ONE ONE ONE ONE
#define HUNDRED FIFTY FIFTY
#define S_256 S_64 S_64 S_64 S_64
#define THOUSAND                                                               \
  HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED      \
      HUNDRED
#define S_1024 S_256 S_256 S_256 S_256
#define S_8192 S_1024 S_1024 S_1024 S_1024 S_1024 S_1024 S_1024 S_1024
#define TEN_THOUSAND                                                           \
  THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND      \
      THOUSAND THOUSAND
#define FIVE_T                                                                 \
  "movss (%0), %1\n"                                                           \
  "movss 4(%0), %1\n"                                                          \
  "movss 8(%0), %1\n"                                                          \
  "movss 12(%0), %1\n"                                                         \
  "movss 16(%0), %1\n"                                                         \
  "add $20, %0\n"
#define TEN_T FIVE_T FIVE_T
#define FIFTY_T TEN_T TEN_T TEN_T TEN_T TEN_T
#define S_64_T FIFTY_T TEN_T ONE_T ONE_T ONE_T ONE_T
#define HUNDRED_T FIFTY_T FIFTY_T
#define S_256_T S_64_T S_64_T S_64_T S_64_T
#define THOUSAND_T                                                             \
  HUNDRED_T HUNDRED_T HUNDRED_T HUNDRED_T HUNDRED_T HUNDRED_T HUNDRED_T        \
      HUNDRED_T HUNDRED_T HUNDRED_T
#define S_1024_T S_256_T S_256_T S_256_T S_256_T
#define S_8192_T                                                               \
  S_1024_T S_1024_T S_1024_T S_1024_T S_1024_T S_1024_T S_1024_T S_1024_T
#define TEN_THOUSAND_T                                                         \
  THOUSAND_T THOUSAND_T THOUSAND_T THOUSAND_T THOUSAND_T THOUSAND_T THOUSAND_T \
      THOUSAND_T THOUSAND_T THOUSAND_T

#define NUMBER_LOADS_T THOUSAND_T
#define NUMBER_LOADS THOUSAND
#define CHUNK_SIZE ((uint64_t)1000)

#define NUM_RUNS_VAL (10000)

#define MEM_SIZE ((MEM_SIZE_KO << 10) / sizeof(uintptr_t))

extern uint32_t NUM_RUNS;
extern uint32_t NUM_WARMUP;
extern uint32_t NUM_REPEAT;

// Size of the buffer
extern uint64_t MEM_SIZE_KO;

extern uint64_t num_CPU;
extern uint8_t TEST;
extern uint8_t SEQ;
extern uint64_t NODE_CORES;
extern uint8_t RAM_MODE;

extern uint64_t L3_SIZE;
extern uint64_t MAX_FREQ;

extern uintptr_t *data_rnd;

#endif // _GLOBALS_H
