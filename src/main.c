#include <assert.h>
#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "c2c_latency.h"
#include "caches_latency.h"
#include "globals.h"
#include "infos.h"
#include "utils.h"

uint32_t NUM_RUNS = NUM_RUNS_VAL;
uint32_t NUM_WARMUP = NUM_RUNS_VAL;

// Size of the buffer
uint64_t MEM_SIZE_KO = (32);

char FILENAME[256] = "log";

typedef enum { PRETTY, CSV } output;
output OUTPUT = PRETTY;
uint32_t NUM_REPEAT = 10;

uint64_t num_CPU = 0;
uintptr_t *data_rnd;

uint8_t RANDOM = 0;
uint8_t STATE = 0;
uint8_t SEQ = 0;
uint8_t RAM_MODE = 0;
uint8_t C2C_LAT = 0;

uint64_t NODE_CORES = -1ul;
uint64_t L3_SIZE = 0;
uint64_t MAX_FREQ = 0;

Agreg_t AGREG = MEDIAN;

void print_results(double *results, char *filename) {
  sort_siblings_cores(results);
  double(*lat)[num_CPU][NUM_REPEAT] = (double(*)[num_CPU][NUM_REPEAT])results;
  if (OUTPUT == PRETTY) {
    printf("+------+");
    for (uint64_t i = 0; i < num_CPU; i++) {
      printf("-------------+");
    }
    printf("\n| Core:|");
    for (uint64_t i = 0; i < num_CPU; i++) {
      printf("%7" PRIu64 ":     |", i);
    }
    printf("\n+------+");
    for (uint64_t i = 0; i < num_CPU; i++) {
      printf("-------------+");
    }
    printf("\n");
    for (uint64_t i = 0; i < num_CPU; i++) {
      printf("|%5" PRIu64 ":|", i);
      for (uint64_t j = 0; j < num_CPU; j++) {
        sort(lat[i][j]);

        if (AGREG == MEDIAN)
          printf("%6.2f [%4.1f]|", median(lat[i][j]),
                 lat[i][j][NUM_REPEAT - 1] - lat[i][j][0]);
        if (AGREG == MIN)
          printf("%6.2f [%4.1f]|", min(lat[i][j]),
                 lat[i][j][NUM_REPEAT - 1] - lat[i][j][0]);
      }
      printf("\n+------+");
      for (uint64_t i = 0; i < num_CPU; i++) {
        printf("-------------+");
      }
      printf("\n");
    }
    printf("\n");
  } else if (OUTPUT == CSV) {
    FILE *file = fopen(filename, "w");
    fprintf(file, "Core");
    for (uint64_t i = 0; i < num_CPU; i++) {
      fprintf(file, ",%" PRIu64, i);
    }
    fprintf(file, "\n");
    for (uint64_t i = 0; i < num_CPU; i++) {
      fprintf(file, "%" PRIu64, i);
      for (uint64_t j = 0; j < num_CPU; j++) {
        sort(lat[i][j]);
        if (AGREG == MEDIAN)
          fprintf(file, ",%6.2f", median(lat[i][j]));
        if (AGREG == MIN)
          fprintf(file, ",%6.2f", min(lat[i][j]));
      }
      fprintf(file, "\n");
    }
    /*fprintf(file, "Max-min:\nCore");
    for(uint64_t i = 0; i<num_CPU; i++) {
            fprintf(file, ",%llu",i);
    }
    fprintf(file, "\n");
    for(uint64_t i = 0; i<num_CPU; i++) {
            fprintf(file, "%llu",i);
            for(uint64_t j = 0; j<num_CPU; j++) {
                    fprintf(file, ",%4.1f",
    lat[i][j][NUM_REPEAT-1]-lat[i][j][0]);
            }
            fprintf(file,"\n");
    }*/
    fclose(file);
  }
}

void parse_options(int argc, char *argv[]) {
  int opt;
  int option_index = 0;
  const struct option longopt[] = {{"random", no_argument, NULL, 'a'},
                                   {"c2c-lat", no_argument, NULL, 'l'},
                                   {"seq", no_argument, NULL, 'q'},
                                   {"cache-transition", no_argument, NULL, 't'},
                                   {"ram", no_argument, NULL, 'r'},
                                   {"cpu", required_argument, NULL, 'c'},
                                   {"cpu-node", required_argument, NULL, 'p'},
                                   {"format", required_argument, NULL, 'f'},
                                   {"load-num", required_argument, NULL, 'd'},
                                   {"num", required_argument, NULL, 'n'},
                                   {"output", required_argument, NULL, 'o'},
                                   {"size", required_argument, NULL, 's'},
                                   {"min", no_argument, NULL, 'm'},
                                   {NULL, 0, NULL, 0}};

  while ((opt = getopt_long(argc, argv, "c:f:d:n:o:s:mrtal", longopt,
                            &option_index)) != -1) {
    switch (opt) {
    case 'a': {
      RANDOM = 1;
      break;
    }
    case 's': {
      MEM_SIZE_KO = strtoll(optarg, NULL, 10);
      break;
    }
    case 'n': {
      NUM_REPEAT = strtoll(optarg, NULL, 10);
      break;
    }
    case 'c': {
      num_CPU = strtoll(optarg, NULL, 10);
      break;
    }
    case 'p': {
      NODE_CORES = strtoll(optarg, NULL, 10);
      break;
    }
    case 'd': {
      NUM_RUNS = strtoll(optarg, NULL, 10);
      // if not in RAM mode
      if (NUM_WARMUP != 0) {
        NUM_WARMUP = NUM_RUNS;
      }
    }
    case 'o': {
      strncpy(FILENAME, optarg, 254);
      FILENAME[255] = '\0';
      break;
    }
    case 't': {
      STATE = 1;
      break;
    }
    case 'r': {
      NUM_RUNS = 1;
      NUM_WARMUP = 0;
      RAM_MODE = 1;
      break;
    }
    case 'm': {
      AGREG = MIN;
      break;
    }
    case 'q': {
      SEQ = 1;
      break;
    }
    case 'f': {
      if (strcmp(optarg, "csv") == 0) {
        OUTPUT = CSV;
        break;
      } else if (strcmp(optarg, "pretty") == 0) {
        OUTPUT = PRETTY;
        break;
      }
    }
    case 'l': {
      C2C_LAT = 1;
      break;
    }
    default: {
      fprintf(stderr,
              "Usage: %s [{-s | --size} <buffer_size>] [{-n | --num} "
              "<number_of_repetitions>] [{-o | --output} <filename>] "
              "[{-f | --format} {csv | pretty}] [{-c | --cpu} <num_cpu>] "
              "[{-p | --cpu-node} <cpu_per_node> [{-t | --cache-transitions}] "
              "[{-m | --min}] [{-r | --ram}] [{-d | --load-num} <num_loads>] "
              "[{-a | --random}] [{-q | --seq}] [{-l | --c2c-lat}] "
              "[{-t | --cache-transition}]\n",
              argv[0]);
      fprintf(stderr, "Where:\n\t<buffer_size> is the size of the requested "
                      "buffer in ko\n");
      fprintf(stderr,
              "\t<number_of_repetitions> is the number repetitions of the "
              "accesses (by chunks of %" PRIu64 ")\n",
              CHUNK_SIZE);
      fprintf(stderr, "\t<filename> the prefix of the CSV files generated when "
                      "using CSV format\n");
      fprintf(stderr, "\t<num_cpu> the number of CPU to use, always use CPU "
                      "number 0 to <num_cpu>-1\n");
      fprintf(stderr,
              "\t<cpu_per_node> the number of pysical CPU per NUMA node\n");
      fprintf(stderr,
              "\t<num_load> is the number that is multiplied by 10 000 to get "
              "the number of loads done during each iteration\n");
      fprintf(stderr, "\t-t only runs the latency tests\n");
      fprintf(stderr, "\t-m uses min() instead of mean() to agregate the the "
                      "different runs\n");
      fprintf(stderr,
              "\t-r optimises the benchmark for RAM (no warmup, no cache)\n");
      fprintf(stderr, "\t-r measures random read latency\n");
      fprintf(stderr, "\t-q measures sequential read latency\n");
      fprintf(stderr, "\t-l measures core-to-core latency\n");
      fprintf(stderr, "\t-t measures cache transition latency\n");
      exit(0);
    }
    }
  }
  if (NODE_CORES == -1ul) {
    NODE_CORES = num_CPU;
  }
}

static void random_access_measures() {
  double *latencies = malloc(num_CPU * num_CPU * sizeof(double) * NUM_REPEAT);
  fprintf(stderr, "Running benchmarks, please wait...\n");
  data_rnd = malloc(sizeof(uintptr_t) * MEM_SIZE);
  for (uint64_t repeat = 0; repeat < NUM_REPEAT; repeat++) {
    init_random(data_rnd);
    fprintf(stderr, "Random Access Data Generation completed\n");
    benchmarking_cache_latency(latencies, init_memory_random, repeat);
  }
  if (OUTPUT == PRETTY)
    printf("Random reads:\n");
  if (OUTPUT == PRETTY)
    printf("Memory read accesses latencies (cycles):\n");
  char buffer[512];
  sprintf(buffer, "%s_rand_reads.csv", FILENAME);
  print_results(latencies, buffer);
  free(data_rnd);
  free(latencies);
}

static void sequential_access_measures() {
  double *latencies = malloc(num_CPU * num_CPU * sizeof(double) * NUM_REPEAT);
  char buffer[512];
  fprintf(stderr, "Measuring latency for sequential accesses\n");
  for (uint64_t repeat = 0; repeat < NUM_REPEAT; repeat++) {
    benchmarking_cache_latency(latencies, init_memory_sequential, repeat);
  }
  if (OUTPUT == PRETTY)
    printf("Sequential reads:\n");
  if (OUTPUT == PRETTY)
    printf("Memory read accesses latencies (cycles):\n");
  sprintf(buffer, "%s_seq_reads.csv", FILENAME);
  print_results(latencies, buffer);

  fprintf(stderr, "Measuring throughputs for sequential accesses\n");
  for (uint64_t repeat = 0; repeat < NUM_REPEAT; repeat++) {
    benchmarking_cache_latency_throughput(latencies, repeat);
  }
  if (OUTPUT == PRETTY)
    printf("Parallel reads:\n");
  if (OUTPUT == PRETTY)
    printf("Memory read accesses latencies (cycles):\n");
  sprintf(buffer, "%s_seq_thr_reads.csv", FILENAME);
  print_results(latencies, buffer);
  free(latencies);
}

static void cache_state_measures() {
  double *latencies =
      malloc(num_CPU * num_CPU * sizeof(double) * NUM_REPEAT * NUM_TRANSITION);
  data_rnd = malloc(sizeof(uintptr_t) * MEM_SIZE);
  char buffer[512];
  fprintf(stderr, "Mesuring cache state transition latencies (assuming MESIF "
                  "protocol), please wait...");
  for (uint64_t repeat = 0; repeat < NUM_REPEAT; repeat++) {
    init_random(data_rnd);
    fprintf(stderr, "Cache State Data Generation completed\n");
    benchmarking_cache_state_transitions(latencies, init_memory_random, repeat);
  }
  if (OUTPUT == PRETTY)
    printf("I -> F:\n");
  if (OUTPUT == PRETTY)
    printf("Memory read accesses latencies (cycles):\n");
  sprintf(buffer, "%s_I_F_reads.csv", FILENAME);
  print_results(latencies, buffer);
  if (OUTPUT == PRETTY)
    printf("I -> E:\n");
  sprintf(buffer, "%s_I_E_reads.csv", FILENAME);
  print_results(latencies + NUM_REPEAT * num_CPU * num_CPU, buffer);
  if (OUTPUT == PRETTY)
    printf("S -> F:\n");
  sprintf(buffer, "%s_S_F_reads.csv", FILENAME);
  print_results(latencies + 2 * (NUM_REPEAT * num_CPU * num_CPU), buffer);
  free(data_rnd);
  free(latencies);
}

static void c2c_latency_measures() {
  char buffer[512];
  double *latencies = malloc(num_CPU * num_CPU * sizeof(double) * NUM_REPEAT);
  fprintf(stderr, "Mesuring core-to-core latencies\n");
  for (uint64_t repeat = 0; repeat < NUM_REPEAT; repeat++) {
    benchmarking_c2c_latency(latencies, repeat);
  }
  if (OUTPUT == PRETTY)
    printf("Core-to-core latencies:\n");
  sprintf(buffer, "%s_c2c.csv", FILENAME);
  print_results(latencies, buffer);
  free(latencies);
}

int main(int argc, char *argv[]) {
  num_CPU = sysconf(_SC_NPROCESSORS_ONLN);
  parse_options(argc, argv);

  if (!RANDOM && !SEQ && !STATE && !C2C_LAT) {
    fprintf(stderr, "No measure selected!\n");
    return 0;
  }

  srand(time(NULL));
  print_info();
  check_scheduler();

  if (RANDOM) {
    random_access_measures();
  }

  if (SEQ) {
    sequential_access_measures();
  }

  if (STATE) {
    cache_state_measures();
  }

  if (C2C_LAT) {
    c2c_latency_measures();
  }
}
