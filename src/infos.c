#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "globals.h"
#include "infos.h"

enum CPU_VENDOR { INTEL, AMD, OTHER };

// From
// https://stackoverflow.com/questions/73418161/using-cpuid-to-get-cache-size-in-ryzen-cpu#73418610
const char *amd_L2_L3_associotivity_str(uint32_t code) {
  switch (code) {
  case 0:
    return "Disabled";
  case 1:
    return "1 way (direct mapped)";
  case 2:
    return "2 way";
  case 4:
    return "4 way";
  case 6:
    return "8 way";
  case 8:
    return "16 way";
  case 9:
    return "Value for all fields should be determined from Fn8000_001D";
  case 10:
    return "32 way";
  case 11:
    return "48 way";
  case 12:
    return "64 way";
  case 13:
    return "96 way";
  case 14:
    return "128 way";
  case 15:
    return "Fully Associative";
  default: {
    fprintf(stderr, "Unknown associativity (code %" PRIu32 ")\n", code);
    return "Unknown";
  }
  }
}

static void get_cache_info_amd() {
  {
    uint32_t eax, ebx, ecx, edx;
    {                   // L1
      eax = 0x80000005; // the specific code of the cpuid instruction for L1

      __asm__("cpuid" // cpuid is the name of the instruction that queries the
                      // info we want
              : "+a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx));

      uint32_t dataCache_size = (ecx >> 24) & 0xFF,
               dataCache_associativity = (ecx >> 16) & 0xFF,
               dataCache_linesPerTag = (ecx >> 8) & 0xFF,
               dataCache_lineSize = ecx & 0xFF;

      uint32_t instrCache_size = (edx >> 24) & 0xFF,
               instrCache_associativity = (edx >> 16) & 0xFF,
               instrCache_linesPerTag = (edx >> 8) & 0xFF,
               instrCache_lineSize = edx & 0xFF;

      printf("L1 Data Cache:\n"
             "\tSize: %d KB\n"
             "\tAssociativity: %d\n"
             "\tLines per Tag: %d\n"
             "\tLine Size: %d B\n"
             "\n"
             "L1 Instruction Cache:\n"
             "\tSize: %d KB\n"
             "\tAssociativity: %d\n"
             "\tLines per Tag: %d\n"
             "\tLine Size: %d B\n\n",
             dataCache_size, dataCache_associativity, dataCache_linesPerTag,
             dataCache_lineSize, instrCache_size, instrCache_associativity,
             instrCache_linesPerTag, instrCache_lineSize);
    }

    {                   // L2
      eax = 0x80000006; // the specific code of the cpuid instruction for L1

      __asm__("cpuid" // cpuid is the name of the instruction that queries the
                      // info we want
              : "+a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx));

      uint32_t L2_size = (ecx >> 16) & 0xFFFF,
               L2_associativity = (ecx >> 12) & 0xF,
               L2_linesPerTag = (ecx >> 8) & 0xF, L2_lineSize = ecx & 0xFF;

      uint32_t L3_size = (edx >> 18) & 0x3FFF,
               L3_associativity = (edx >> 12) & 0xF,
               L3_linesPerTag = (edx >> 8) & 0xF,
               L3_lineSize = (edx >> 0) & 0xFF;

      printf("L2 Cache:\n"
             "\tSize: %d KB\n"
             "\tAssociativity: %s\n"
             "\tLines per Tag: %d\n"
             "\tLine Size: %d B\n"
             "\n"
             "L3 Cache:\n"
             "\tSize: %d KB\n"
             "\tAssociativity: %s\n"
             "\tLines per Tag: %d\n"
             "\tLine Size: %d B\n\n",
             L2_size, amd_L2_L3_associotivity_str(L2_associativity),
             L2_linesPerTag, L2_lineSize, L3_size * 512,
             amd_L2_L3_associotivity_str(L3_associativity), L3_linesPerTag,
             L3_lineSize);
    }
  }
}

static void get_cache_info_intel() {
  for (int i = 0; i < 32; i++) {

    // Variables to hold the contents of the 4 i386 legacy registers
    uint32_t eax, ebx, ecx, edx;

    eax = 4; // get cache info
    ecx = i; // cache id

    __asm__(
        "cpuid"     // call i386 cpuid instruction
        : "+a"(eax) // contains the cpuid command code, 4 for cache query
          ,
          "=b"(ebx), "+c"(ecx) // contains the cache id
          ,
          "=d"(edx)); // generates output in 4 registers eax, ebx, ecx and edx

    // taken from http://download.intel.com/products/processor/manual/325462.pdf
    // Vol. 2A 3-149
    int cache_type = eax & 0x1F;

    if (cache_type == 0) // end of valid cache identifiers
      break;

    char *cache_type_string;
    switch (cache_type) {
    case 1:
      cache_type_string = "Data Cache";
      break;
    case 2:
      cache_type_string = "Instruction Cache";
      break;
    case 3:
      cache_type_string = "Unified Cache";
      break;
    default:
      cache_type_string = "Unknown Type Cache";
      break;
    }

    int cache_level = (eax >>= 5) & 0x7;

    int cache_is_self_initializing =
        (eax >>= 3) & 0x1; // does not need SW initialization
    int cache_is_fully_associative = (eax >>= 1) & 0x1;

    // taken from http://download.intel.com/products/processor/manual/325462.pdf
    // 3-166 Vol. 2A ebx contains 3 integers of 10, 10 and 12 bits respectively
    unsigned int cache_sets = ecx + 1;
    unsigned int cache_coherency_line_size = (ebx & 0xFFF) + 1;
    unsigned int cache_physical_line_partitions = ((ebx >>= 12) & 0x3FF) + 1;
    unsigned int cache_ways_of_associativity = ((ebx >>= 10) & 0x3FF) + 1;

    // Total cache size is the product
    size_t cache_total_size = cache_ways_of_associativity *
                              cache_physical_line_partitions *
                              cache_coherency_line_size * cache_sets;

    fprintf(stderr,
            "Cache ID %d:\n"
            "- Level: %d\n"
            "- Type: %s\n"
            "- Sets: %d\n"
            "- System Coherency Line Size: %d bytes\n"
            "- Physical Line partitions: %d\n"
            "- Ways of associativity: %d\n"
            "- Total Size: %zu bytes (%zu kB)\n"
            "- Is fully associative: %s\n"
            "- Is Self Initializing: %s\n"
            "\n",
            i, cache_level, cache_type_string, cache_sets,
            cache_coherency_line_size, cache_physical_line_partitions,
            cache_ways_of_associativity, cache_total_size,
            cache_total_size >> 10,
            cache_is_fully_associative ? "true" : "false",
            cache_is_self_initializing ? "true" : "false");
    if (cache_level == 3)
      L3_SIZE = cache_total_size;
  }
}

// returns true on an Intel processor, false on anything else
static int get_cpu_vendor() {
  int id_str; // The first four characters of the vendor ID string

  __asm__ volatile(
      "cpuid"
      : // run the cpuid instruction with...
      "=c"(id_str)
      : // id_str set to the value of EBX after cpuid runs...
      "a"(0)
      : // and EAX set to 0 to run the proper cpuid function.
      "ebx",
      "edx"); // cpuid clobbers EBX and EDX, in addition to EAX and ECX.

  switch (id_str) {
  case (0x6c65746e): { // letn. little endian clobbering of GenuineI[ntel]
    return INTEL;
  }
  case (0x444d4163): { // letn. little endian clobbering of Authenti[cAMD]
    return AMD;
  }
  default: {
    fprintf(stderr, "Unknow CPU vendor: 0x%x\n", id_str);
    return OTHER;
  }
  }
}

// From
// https://stackoverflow.com/questions/21369381/measuring-cache-latencies#21463541
void print_info() {
  fprintf(stderr, "Number of CPUs =        %7" PRIu64 "\n", num_CPU);
  fprintf(stderr, "Buffer size =           %7" PRIu64 " kB\n", MEM_SIZE_KO);
  fprintf(stderr, "Number of accesses =    %7" PRIu64 "\n", CHUNK_SIZE);
  fprintf(stderr, "Number of repetitions = %7" PRIu32 "\n", NUM_REPEAT);
  fprintf(stderr, "\n");

  switch (get_cpu_vendor()) {
  case INTEL: {
    get_cache_info_intel();
    break;
  }
  case AMD: {
    get_cache_info_amd();
    break;
  }
  default: {
    fprintf(stderr, "Unknown CPU type\n");
  }
  }
}
