#include <string.h>
#include <sys/mman.h>

#include "caches_latency.h"
#include "globals.h"
#include "utils.h"

// Initialise a table with a random one-cycle permutation
// of the array [0, sizeof(uintptr_t), ... , (MEM_SIZE-1)*sizeof(uintptr_t)]
// corresponding to the offset of the next case to load when testing random
// reads
void init_random(uintptr_t *data) {
  for (uint64_t i = 0; i < MEM_SIZE; ++i)
    data[i] = -1;
  uint64_t first = 0;
  uint64_t former = first;
  // No need to generate more than whatr will actually be loaded
  uint64_t upper = ((MEM_SIZE - 1) > (2 * (NUM_RUNS * CHUNK_SIZE) + 1))
                       ? (2 * ((NUM_RUNS * CHUNK_SIZE) + 1))
                       : (MEM_SIZE - 1);
  for (uint64_t i = 0; i < upper; ++i) {
    uint64_t j = (randl()) % MEM_SIZE;
    while (data[j] != -1 || j == first || j == former) {
      j = (j + 1) % MEM_SIZE;
    }
    data[former] = j * sizeof(uintptr_t);
    former = j;
  }
  data[former] = first * sizeof(uintptr_t);
}

// Update the random read data such that table[i] = address
// of the next j to access (i.e. just add the offset)
static void update_random(uintptr_t *data) {
  for (uint64_t i = 0; i < MEM_SIZE; ++i) {
    data[i] += (uintptr_t)data;
  }
}

uintptr_t *init_memory_random() {
  uintptr_t *data = allocate(sizeof(uintptr_t) * MEM_SIZE);

  memcpy(data, data_rnd, MEM_SIZE * sizeof(uintptr_t));
  update_random(data);
  return data;
}

uintptr_t *init_memory_sequential() {
  uintptr_t *data = allocate(sizeof(uintptr_t) * MEM_SIZE);

  for (uint64_t i = 0; i < MEM_SIZE; ++i)
    data[i] = sizeof(uintptr_t) * ((i + 1) % MEM_SIZE) + (uintptr_t)data;

  return data;
}

static double read_memory_warmup(uintptr_t *data) {
  uint64_t tot_loads = 0;
  uint32_t loads = 0;
  uintptr_t load = (uintptr_t)data;
  // inspired from
  // https://stackoverflow.com/questions/21369381/measuring-cache-latencies#21463541
  // Cache warm-up
  load = (uintptr_t)data;
  for (uint32_t warm = 0; warm < NUM_WARMUP; warm++) {
    int32_t tmp0, tmp1, ecx, edx;
    __asm__ volatile("movq %6, %5\n"
                     "rdtscp\n"
                     "movl %%edx, %1\n"
                     "movl %%eax, %0\n" NUMBER_LOADS "rdtscp\n"
                     "subl %1, %%edx\n"
                     "sbbl %0, %%eax\n"
                     : "=r"(tmp0), "=r"(tmp1), "=a"(loads), "=c"(ecx),
                       "=d"(edx), "=r"(load)
                     : "r"(load));
  }
  // Real loads
  load = (uintptr_t)data;
  for (uint64_t run = 0; run < NUM_RUNS; run++) {
    int32_t tmp0, tmp1, ecx, edx;
    __asm__ volatile("movq %6, %5\n"
                     "rdtscp\n"
                     "movl %%edx, %1\n"
                     "movl %%eax, %0\n" NUMBER_LOADS "rdtscp\n"
                     "subl %1, %%edx\n"
                     "sbbl %0, %%eax\n"
                     : "=r"(tmp0), "=r"(tmp1), "=a"(loads), "=c"(ecx),
                       "=d"(edx), "=r"(load)
                     : "r"(load));
    tot_loads += loads;
  }
  return ((double)tot_loads) / (CHUNK_SIZE * NUM_RUNS);
}

static double read_memory_no_warmup(uintptr_t *data) {
  uint64_t tot_loads = 0;
  uint32_t loads = 0;
  uintptr_t load = (uintptr_t)data;

  // Real loads
  for (uint64_t run = 0; run < NUM_RUNS; run++) {
    int32_t tmp0, tmp1, ecx, edx;
    __asm__ volatile("movq %6, %5\n"
                     "rdtscp\n"
                     "movl %%edx, %1\n"
                     "movl %%eax, %0\n" NUMBER_LOADS "rdtscp\n"
                     "subl %1, %%edx\n"
                     "sbbl %0, %%eax\n"
                     : "=r"(tmp0), "=r"(tmp1), "=a"(loads), "=c"(ecx),
                       "=d"(edx), "=r"(load)
                     : "r"(load));
    tot_loads += loads;
  }

  return (((double)tot_loads) / (CHUNK_SIZE * NUM_RUNS));
}

static double read_memory_throughput_warmup(const uintptr_t *data) {
  // Cache warm-up
  for (uint32_t warm = 0; warm < NUM_WARMUP; warm++) {
    __asm__ volatile(NUMBER_LOADS_T : : "r"(data), "x"(0.));
  }
  // Real loads
  uint64_t start = rdtscp();
  for (uint64_t run = 0; run < NUM_RUNS; run++) {
    __asm__ volatile(NUMBER_LOADS_T : : "r"(data), "x"(0.));
  }
  uint64_t end = rdtscp();
  uint64_t tot_loads = end - start;

  return (((double)tot_loads) / (CHUNK_SIZE * NUM_RUNS));
}

void benchmarking_cache_latency(double *latencies, uintptr_t *init_func(void),
                                uint64_t repeat) {
  double(*lat)[num_CPU][NUM_REPEAT] = (double(*)[num_CPU][NUM_REPEAT])latencies;
  for (uint64_t i = 0; i < num_CPU; i++) {
    for (uint64_t j = 0; j < num_CPU; j++) {
      uintptr_t *data;
      pin_cpu(i);
      data = init_func();
      uint64_t proc = j - (j % NODE_CORES) + ((j + repeat) % NODE_CORES);
      if (RAM_MODE)
        scramble_cache();
      pin_cpu(proc);
      lat[i][proc][repeat] = read_memory_warmup(data);
      munmap(data, MEM_SIZE * sizeof(uintptr_t));
    }
  }
}

void benchmarking_cache_latency_throughput(double *latencies, uint64_t repeat) {
  double(*lat)[num_CPU][NUM_REPEAT] = (double(*)[num_CPU][NUM_REPEAT])latencies;
  for (uint64_t i = 0; i < num_CPU; i++) {
    for (uint64_t j = 0; j < num_CPU; j++) {
      uintptr_t *data;
      pin_cpu(i);
      data = init_memory_sequential();
      uint64_t proc = j - (j % NODE_CORES) + ((j + repeat) % NODE_CORES);
      if (RAM_MODE)
        scramble_cache();
      pin_cpu(proc);
      lat[i][proc][repeat] = read_memory_throughput_warmup(data);
      munmap(data, MEM_SIZE * sizeof(uintptr_t));
    }
  }
}

void benchmarking_cache_state_transitions(double *latencies,
                                          uintptr_t *init_func(void),
                                          uint64_t repeat) {
  double(*lat)[num_CPU][num_CPU][NUM_REPEAT] =
      (double(*)[num_CPU][num_CPU][NUM_REPEAT])latencies;
  for (uint64_t i = 0; i < num_CPU; i++) {
    for (uint64_t j = 0; j < num_CPU; j++) {
      uintptr_t *data;
      pin_cpu(j);
      data = init_func();
      // j reads memory: I->F
      lat[I_F][i][j][repeat] = read_memory_no_warmup(data);
      // i reads memory again: S->F
      pin_cpu(i);
      read_memory_no_warmup(data);
      // j reads memory again: S->F
      pin_cpu(j);
      lat[S_F][i][j][repeat] = read_memory_no_warmup(data);
      // i clear its cache
      pin_cpu(i);
      scramble_cache();
      // j clear its cache
      pin_cpu(j);
      scramble_cache();
      // j reads memory again: I->E
      lat[I_E][i][j][repeat] = read_memory_no_warmup(data);
      munmap(data, MEM_SIZE * sizeof(uintptr_t));
    }
  }
}
