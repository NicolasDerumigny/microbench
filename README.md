Microbench: a simple NUMA cache latency measurer
================================================

# Author
	- Nicolas Derumigny (nderumigny@gmail.com)

# Description
Simple bechmark to measure cache latencies depending on the core allocating memory. It generates a buffer composed of the next adress to load so that there is only one cycle in the whole buffer, and that the next address to be loaded is as random as possible. The time in cycle taken to execute a constant number of hardcoded one-word `mov` instructions is reported by `rdtscp`, then reported to one instruction. The software also supports latency measurements of cache state switching latency (still in beta).
The data can be visualized using R.

# Dependencies
	- A working C compiler
	- R with packages `ggplot2`, `tidyverse` and `rmarkdown` for fancy visualisation of CSV files

# Build
`make`

# Run
`./bench [{-s | --size} <buffer_size>] [{-n | --num} <number_of_repetitions>] [{-o | --output} <filename>] [{-f | --format} {csv | pretty}] [{-c | --cpu} <num_cpu>] [{-p | --cpu-node} <cpu_per_node> [{-t | --test}] [{-m | --min}] [{-r | --ram}] [{-l | --load-num} <num_loads>`

With:

	- `{-s | --size} <buffer_size>`                  The size of the buffer
	- `{-n | --num} <number_of_repetitions>`         The number of repetition of each measurements
	- `{-l | --load-num <num_loads>                  The number of chunk of 10 000 loads done for each repetition
	- `{-m | --min}`                                 Output the min of all the repetitions instead of the median
	- `{-f | --format} {csv | pretty}`               Either save the results in a CSV file or print them directly on stdout in a fancy table style
	- `{-o | --output} <filename>`                   When using CSV format, specifies the non-prefixed name of the output file
	- `{-c | --cpu} <num_cpu>`                       Limit the bench to the `num_cpu` first CPUs
	- `{-p | --cpu-node} <cpu_per_node>`             Specify the number of cpu per NUMA node
	- `{-t | --test}`                                Only run the cache latency benchmark (*test mode*)
	- `{-r | --ram}`                                 Benchmark RAM: use test mode and do not use any warm-up sequence
	- `{--seq}`                                      Benchmark sequencial read time

# FAQ
*I'm flooded with messages concerning the governor, what can I do?*

To avoid variance due to DVFS (i.e. boost frequency), you prefer to freeze the CPU frequency to its non-boost frequency. For that, you have to set the governor to *performance*, look on your distribution's instructions. If you cannot, do not worry, this would not prevent the benchmark from being run.


*I have an error message related to sched_setscheduler, what can I do?*

Still in order to stabilized the result, the scheduler is changed to a FIFO one. This requieres to have root access, so do not worry about it if you work only in userkand. This also do not prevent the benchmark from being run.

*I'm targetting RAM and the `-r` and `-t` results differs, why?*

The -r option disable warm-ups, so there are two possible problems here. Either the size of your buffer is insufficient and you are experiencing cache effects in the `-t` run; or the boost is not properly disabled and your CPU frequency is changed dynamically in a different way in the two executions.

# TODO
	- Add support for cache size detection on AMD
	- Add automatic detection of NUMA nodes
	- Add more cache coherency protocols
	- Quiet mode that do not display the errors
	- Add sequential measure for reverse order of access